import asyncio
from logging import Logger
from typing import Dict, Union

from telethon import events, utils
from kantek.utils.pluginmgr import k
from kantek.config import Config
from kantek.vendor import lazybot
import html
from kantex import html as kantexHtml

config = Config()


class LogBot:
    """
    Logbot to log crap to the Telegram chat. 
    """
    # docstring cus pylint btw
    def __init__(self, bot_token: str, channel_id: Union[str, int]) -> None:
        self.bot = lazybot.Bot(bot_token)
        self.channel_id = channel_id
        # might want to make this a instance variable if its safe to do so
        super().__init__()

    async def connect(self):
        self.me: Dict[str, Union[bool, str, int]] = await self.bot.get_me()
        if not self.me['ok']:
            Logger.warning('Got Error: %s %s '
                           'from the bot API. '
                           'Check if your `log_bot_token` in the config is correct.',
                           self.me.get("error_code"), self.me.get("description"))

    def sendMessage(self, msg: str, parse_mode: str) -> None:
        """Send the log message to the specified Telegram channel."""
        asyncio.ensure_future(self.bot.send_message(
            chat_id=self.channel_id,
            text=msg,
            parse_mode=parse_mode,
            disable_web_page_preview=True))


@k.event(events.chataction.ChatAction(),
         name='mitgliedslogger')  # Module to log user joins or user added, by Dank-del (t.me/dank_as_fuck) // (misaki@eagleunion.tk)
async def log_user_join(event):
    """
    Automatically logs user joins or added, to log chat
    """
    if not event.user_joined and not event.user_added:
        return

    data = await event.get_user()
    chat = await event.client.get_entity(event.chat_id)

    kmsg = kantexHtml.KanTeXDocument(
        kantexHtml.Section(
            "User Joined in {}".format(chat.title)
            if event.user_joined
            else "User Added to {}".format(chat.title),
            kantexHtml.SubSection(
                kantexHtml.KeyValueItem("chat_id", kantexHtml.Code(chat.id)),
                kantexHtml.KeyValueItem(
                    "chat_username", kantexHtml.Code(chat.username or "None")
                ),
                kantexHtml.KeyValueItem(
                    "first_name", kantexHtml.Code(data.first_name)
                ),
                kantexHtml.KeyValueItem(
                    "last_name", kantexHtml.Code(data.last_name or "None")
                ),
                kantexHtml.KeyValueItem(
                    "username", kantexHtml.Code(data.username or "None")
                ),
                kantexHtml.KeyValueItem("user_id", kantexHtml.Code(data.id)),
            ),
        )
    )

    if event.user_added:
        adder = event.added_by
        kmsg.extend(
            [
                kantexHtml.SubSection(
                    "Added by",
                    kantexHtml.KeyValueItem(
                        "first_name", kantexHtml.Code(adder.first_name)
                    ),
                    kantexHtml.KeyValueItem(
                        "last_name", kantexHtml.Code(adder.last_name or "None")
                    ),
                    kantexHtml.KeyValueItem(
                        "username", kantexHtml.Code(adder.username or "None")
                    ),
                    kantexHtml.KeyValueItem(
                        "user_id", kantexHtml.Code(adder.id)
                    ),
                )
            ]
        )

    bot = LogBot(config.log_bot_token, config.log_channel_id)
    await bot.connect()
    bot.sendMessage(msg=str(kmsg), parse_mode="html")


admin_report_no_reportee = '''<b>Report Event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={reporter.id}'>Reporter</a>: {reporter_name} (<code>{reporter.id}</code>)
Remark: <code>{remark}</code>'''

admin_report = '''<b>Report Event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={reporter.id}'>Reporter</a>: {reporter_name} (<code>{reporter.id}</code>)
<a href='tg://user?id={reportee.id}'>Reportee</a>: {reportee_name} (<code>{reportee.id}</code>)
Remark: <code>{remark}</code>
Reported Message: <code>{reported_message}</code>'''


@k.event(events.NewMessage(pattern=r'@admins?|^[/\.!#](?:report|admins?)(?:$|\W)', incoming=True), name='reportlogger')
async def report_logger(event):
    """
    Automatically logs admin reports, to log chat
    """
    bot = LogBot(config.log_bot_token, config.log_channel_id)
    await bot.connect()
    if event.is_private:
        return
    if event.chat_id == config.log_channel_id:
        #        No recursion please
        return
    reporter = await event.get_sender()
    reporter_name = html.escape(utils.get_display_name(reporter))
    chat = await event.get_chat()
    if not getattr(chat, 'username', None):
        unmark_cid = await event.client.get_peer_id(chat.id, False)
        link = f'https://t.me/c/{unmark_cid}/{event.id}'
    else:
        link = f'https://t.me/{chat.username}/{event.id}'
    if event.is_reply:
        r = await event.get_reply_message()
        try:
            reportee = await r.get_sender()
            reportee_name = html.escape(utils.get_display_name(reportee))
        except AttributeError:
            bot.sendMessage(msg=str(admin_report_no_reportee.format(
                reporter=reporter, chat=chat, event=event,
                remark=html.escape(str(event.text)), link=link,
                reporter_name=reporter_name)), parse_mode="html")
            return

        # await event.client.send_message(config.log_channel_id, , link_preview=False, parse_mode="html")

        bot.sendMessage(msg=str(admin_report.format(
            reporter=reporter, reportee=reportee, chat=chat, event=event, r=r,
            remark=html.escape(str(event.text)), link=link,
            reported_message=html.escape(str(r.text)), reporter_name=reporter_name,
            reportee_name=reportee_name)), parse_mode="html")
    else:
        bot.sendMessage(msg=str(admin_report_no_reportee.format(
            reporter=reporter, chat=chat, event=event, remark=html.escape(str(event.text)),
            link=link, reporter_name=reporter_name)), parse_mode="html")
        # await event.client.send_message(config.log_channel_id, , link_preview=False, parse_mode="html")


admin_banned_no_reply = '''<b>Ban event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={banner.id}'>Banned by</a>: {banner_name} (<code>{banner.id}</code>)
Remark: <code>{remark}</code>'''

admin_banned = '''<b>Ban event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={banner.id}'>Banned by</a>: {banner_name} (<code>{banner.id}</code>)
<a href='tg://user?id={banee.id}'>Banned User</a>: {banee_name} (<code>{banee.id}</code>)
Remark: <code>{remark}</code>
Ban Message: <code>{ban_message}</code>'''


@k.event(events.NewMessage(pattern=r'[!/\.,\*]ban', incoming=True), name='banlogger')
async def ban_logger(event):
    """
    Automatically logs ban events, to log chat
    """
    bot = LogBot(config.log_bot_token, config.log_channel_id)
    await bot.connect()
    if event.is_private:
        return
    if event.chat_id == config.log_channel_id:
        #        No recursion please
        return
    banner = await event.get_sender()
    banner_name = html.escape(utils.get_display_name(banner))
    chat = await event.get_chat()
    if not getattr(chat, 'username', None):
        unmark_cid = await event.client.get_peer_id(chat.id, False)
        link = f'https://t.me/c/{unmark_cid}/{event.id}'
    else:
        link = f'https://t.me/{chat.username}/{event.id}'
    if event.is_reply:
        r = await event.get_reply_message()
        try:
            banee = await r.get_sender()
            banee_name = html.escape(utils.get_display_name(banee))
        except AttributeError:
            bot.sendMessage(msg=str(admin_banned_no_reply.format(
                banner=banner, chat=chat, event=event,
                remark=html.escape(str(event.text)), link=link,
                banner_name=banner_name)),
                parse_mode="html")
            return

        bot.sendMessage(msg=str(admin_banned.format(
            banner=banner, banee=banee, chat=chat, event=event, r=r,
            remark=html.escape(str(event.text)), link=link,
            ban_message=html.escape(str(r.text)), banner_name=banner_name,
            banee_name=banee_name)), parse_mode="html")
    else:
        bot.sendMessage(msg=str(admin_banned_no_reply.format(
            banner=banner, chat=chat, event=event, remark=html.escape(str(event.text)),
            link=link, banner_name=banner_name)), parse_mode="html")


admin_unbanned_no_reply = '''<b>Unban Event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={unbanner.id}'>Unbanned by</a>: {unbanner_name} (<code>{unbanner.id}</code>)
Remark: <code>{remark}</code>'''

admin_unbanned = '''<b>Unban Event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={unbanner.id}'>Unbanned by</a>: {unbanner_name} (<code>{unbanner.id}</code>)
<a href='tg://user?id={unbanee.id}'>Banned User</a>: {unbanee_name} (<code>{unbanee.id}</code>)
Remark: <code>{remark}</code>
Unban Message: <code>{unban_message}</code>'''


@k.event(events.NewMessage(pattern=r'[!/\.,\*]unban', incoming=True), name='unbanlogger')
async def unban_logger(event):
    """
    Automatically logs unban events, to log chat
    """
    bot = LogBot(config.log_bot_token, config.log_channel_id)
    await bot.connect()
    if event.is_private:
        return
    if event.chat_id == config.log_channel_id:
        #        No recursion please
        return
    unbanner = await event.get_sender()
    unbanner_name = html.escape(utils.get_display_name(unbanner))
    chat = await event.get_chat()
    if not getattr(chat, 'username', None):
        unmark_cid = await event.client.get_peer_id(chat.id, False)
        link = f'https://t.me/c/{unmark_cid}/{event.id}'
    else:
        link = f'https://t.me/{chat.username}/{event.id}'
    if event.is_reply:
        r = await event.get_reply_message()
        try:
            unbanee = await r.get_sender()
            unbanee_name = html.escape(utils.get_display_name(unbanee))
        except AttributeError:
            bot.sendMessage(msg=str(admin_unbanned_no_reply.format(
                unbanner=unbanner, chat=chat, event=event,
                remark=html.escape(str(event.text)), link=link,
                unbanner_name=unbanner_name)),
                parse_mode="html")
            return

        bot.sendMessage(msg=str(admin_unbanned.format(
            unbanner=unbanner, unbanee=unbanee, chat=chat, event=event, r=r,
            remark=html.escape(str(event.text)), link=link,
            unban_message=html.escape(str(r.text)), unbanner_name=unbanner_name,
            unbanee_name=unbanee_name)), parse_mode="html")
    else:
        bot.sendMessage(msg=str(admin_unbanned_no_reply.format(
            unbanner=unbanner, chat=chat, event=event, remark=html.escape(str(event.text)),
            link=link, unbanner_name=unbanner_name)), parse_mode="html")


muted_no_reply = '''<b>Mute event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={muter.id}'>Muted by</a>: {muter_name} (<code>{muter.id}</code>)
Remark: <code>{remark}</code>'''

muted = '''<b>Mute event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={muter.id}'>Muted by</a>: {muter_name} (<code>{muter.id}</code>)
<a href='tg://user?id={muteee.id}'>Muted User</a>: {muteee_name} (<code>{muteee.id}</code>)
Remark: <code>{remark}</code>
Mute Message: <code>{mute_message}</code>'''


@k.event(events.NewMessage(pattern=r'[!/\.,\*]mute', incoming=True), name='mutelogger')
async def mute_logger(event):
    """
    Automatically logs mute events, to log chat
    """
    bot = LogBot(config.log_bot_token, config.log_channel_id)
    await bot.connect()
    if event.is_private:
        return
    if event.chat_id == config.log_channel_id:
        #        No recursion please
        return
    muter = await event.get_sender()
    muter_name = html.escape(utils.get_display_name(muter))
    chat = await event.get_chat()
    if not getattr(chat, 'username', None):
        unmark_cid = await event.client.get_peer_id(chat.id, False)
        link = f'https://t.me/c/{unmark_cid}/{event.id}'
    else:
        link = f'https://t.me/{chat.username}/{event.id}'
    if event.is_reply:
        r = await event.get_reply_message()
        try:
            muteee = await r.get_sender()
            muteee_name = html.escape(utils.get_display_name(muteee))
        except AttributeError:
            bot.sendMessage(msg=str(muted_no_reply.format(
                muter=muter, chat=chat, event=event,
                remark=html.escape(str(event.text)), link=link,
                muter_name=muter_name)),
                parse_mode="html")
            return

        bot.sendMessage(msg=str(muted.format(
            muter=muter, muteee=muteee, chat=chat, event=event, r=r,
            remark=html.escape(str(event.text)), link=link,
            mute_message=html.escape(str(r.text)), muter_name=muter_name,
            muteee_name=muteee_name)), parse_mode="html")
    else:
        bot.sendMessage(msg=str(muted_no_reply.format(
            muter=muter, chat=chat, event=event, remark=html.escape(str(event.text)),
            link=link, muter_name=muter_name)), parse_mode="html")


unmuted_no_reply = '''<b>Unmute event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={unmuter.id}'>Unmuted by</a>: {unmuter_name} (<code>{unmuter.id}</code>)
Remark: <code>{remark}</code>'''

unmuted = '''<b>Unmute event</b>

<a href='{link}'>Chat</a>: {chat.title} (<code>{chat.id}</code>)
<a href='tg://user?id={unmuter.id}'>Unmuted by</a>: {unmuter_name} (<code>{unmuter.id}</code>)
<a href='tg://user?id={unmuteee.id}'>Unmuted User</a>: {unmuteee_name} (<code>{unmuteee.id}</code>)
Remark: <code>{remark}</code>
Unmute Message: <code>{unmute_message}</code>'''


@k.event(events.NewMessage(pattern=r'[!/\.,\*]unmute', incoming=True), name='unmutelogger')
async def unmute_logger(event):
    """
    Automatically logs unmute events, to log chat
    """
    bot = LogBot(config.log_bot_token, config.log_channel_id)
    await bot.connect()
    if event.is_private:
        return
    if event.chat_id == config.log_channel_id:
        #        No recursion please
        return
    unmuter = await event.get_sender()
    unmuter_name = html.escape(utils.get_display_name(unmuter))
    chat = await event.get_chat()
    if not getattr(chat, 'username', None):
        unmark_cid = await event.client.get_peer_id(chat.id, False)
        link = f'https://t.me/c/{unmark_cid}/{event.id}'
    else:
        link = f'https://t.me/{chat.username}/{event.id}'
    if event.is_reply:
        r = await event.get_reply_message()
        try:
            unmuteee = await r.get_sender()
            unmuteee_name = html.escape(utils.get_display_name(unmuteee))
        except AttributeError:
            bot.sendMessage(msg=str(unmuted_no_reply.format(
                unmuter=unmuter, chat=chat, event=event,
                remark=html.escape(str(event.text)), link=link,
                unmuter_name=unmuter_name)),
                parse_mode="html")
            return

        bot.sendMessage(msg=str(unmuted.format(
            unmuter=unmuter, unmuteee=unmuteee, chat=chat, event=event, r=r,
            remark=html.escape(str(event.text)), link=link,
            unmute_message=html.escape(str(r.text)), unmuter_name=unmuter_name,
            unmuteee_name=unmuteee_name)), parse_mode="html")
    else:
        bot.sendMessage(msg=str(unmuted_no_reply.format(
            unmuter=unmuter, chat=chat, event=event, remark=html.escape(str(event.text)),
            link=link, unmuter_name=unmuter_name)), parse_mode="html")
